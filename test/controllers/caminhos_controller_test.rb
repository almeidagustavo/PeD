require 'test_helper'

class CaminhosControllerTest < ActionDispatch::IntegrationTest
  setup do
    @caminho = caminhos(:one)
  end

  test "should get index" do
    get caminhos_url
    assert_response :success
  end

  test "should get new" do
    get new_caminho_url
    assert_response :success
  end

  test "should create caminho" do
    assert_difference('Caminho.count') do
      post caminhos_url, params: { caminho: {  } }
    end

    assert_redirected_to caminho_url(Caminho.last)
  end

  test "should show caminho" do
    get caminho_url(@caminho)
    assert_response :success
  end

  test "should get edit" do
    get edit_caminho_url(@caminho)
    assert_response :success
  end

  test "should update caminho" do
    patch caminho_url(@caminho), params: { caminho: {  } }
    assert_redirected_to caminho_url(@caminho)
  end

  test "should destroy caminho" do
    assert_difference('Caminho.count', -1) do
      delete caminho_url(@caminho)
    end

    assert_redirected_to caminhos_url
  end
end
