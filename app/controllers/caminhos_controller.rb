class CaminhosController < ApplicationController
  before_action :set_caminho, only: [:show, :edit, :update, :destroy]

  # GET /caminhos
  # GET /caminhos.json
  def index
    @caminhos = Caminho.all
  end

  # GET /caminhos/1
  # GET /caminhos/1.json
  def show
  end

  # GET /caminhos/new
  def new
    @caminho = Caminho.new
  end

  # GET /caminhos/1/edit
  def edit
  end

  # POST /caminhos
  # POST /caminhos.json
  def create
    @caminho = Caminho.new(caminho_params)

    respond_to do |format|
      if @caminho.save
        format.html { redirect_to @caminho, notice: 'Caminho was successfully created.' }
        format.json { render :show, status: :created, location: @caminho }
      else
        format.html { render :new }
        format.json { render json: @caminho.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /caminhos/1
  # PATCH/PUT /caminhos/1.json
  def update
    respond_to do |format|
      if @caminho.update(caminho_params)
        format.html { redirect_to @caminho, notice: 'Caminho was successfully updated.' }
        format.json { render :show, status: :ok, location: @caminho }
      else
        format.html { render :edit }
        format.json { render json: @caminho.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /caminhos/1
  # DELETE /caminhos/1.json
  def destroy
    @caminho.destroy
    respond_to do |format|
      format.html { redirect_to caminhos_url, notice: 'Caminho was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_caminho
      @caminho = Caminho.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def caminho_params
      params.fetch(:caminho, {})
    end
end
